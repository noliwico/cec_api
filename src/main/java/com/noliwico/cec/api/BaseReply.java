package com.noliwico.cec.api;

public class BaseReply {
	private int errorCode;
	private String errorMessage;
	
	public BaseReply() {
		this.errorCode = 0;
	}
	
	public BaseReply(int errorCode, String errorMessage) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}
	
	public int getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	

}
