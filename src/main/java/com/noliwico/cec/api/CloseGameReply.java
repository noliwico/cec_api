package com.noliwico.cec.api;

public class CloseGameReply extends BaseReply {
	
	private String gameId;
	private boolean closeSuccess;
	
	public CloseGameReply() {
		super();
	}
	


	public CloseGameReply(String gameId, boolean closeSuccess) {
		super();
		this.gameId = gameId;
		this.closeSuccess = closeSuccess;
	}



	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}

	public boolean isCloseSuccess() {
		return closeSuccess;
	}
	public void setCloseSuccess(boolean closeSuccess) {
		this.closeSuccess = closeSuccess;
	}
	
	

}
