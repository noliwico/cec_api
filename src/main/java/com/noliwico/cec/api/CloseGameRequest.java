package com.noliwico.cec.api;

public class CloseGameRequest {
	private String gameId;
	private String adminId;
	
	public CloseGameRequest() {
		
	}

	public CloseGameRequest(String gameId, String adminId) {
		super();
		this.gameId = gameId;
		this.adminId = adminId;
	}

	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}

	public String getAdminId() {
		return adminId;
	}

	public void setAdminId(String adminId) {
		this.adminId = adminId;
	}
	

	
	

}
