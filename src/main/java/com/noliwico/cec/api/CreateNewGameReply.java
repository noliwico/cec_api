
package com.noliwico.cec.api;

public class CreateNewGameReply extends BaseReply{
		private String displayName;
		private int maxPlayers;
		private String gameId;
		private String adminId;
		
		public CreateNewGameReply() {
			super();
		}

		
		public CreateNewGameReply(String displayName, int maxPlayers, String gameId, String adminId) {
			super();
			this.displayName = displayName;
			this.maxPlayers = maxPlayers;
			this.gameId = gameId;
			this.adminId = adminId;
		}


		public String getDisplayName() {
			return displayName;
		}
		public void setDisplayName(String displayName) {
			this.displayName = displayName;
		}
		public int getMaxPlayers() {
			return maxPlayers;
		}
		public void setMaxPlayers(int maxPlayers) {
			this.maxPlayers = maxPlayers;
		}

		public String getGameId() {
			return gameId;
		}

		public void setGameId(String gameId) {
			this.gameId = gameId;
		}

		public String getAdminId() {
			return adminId;
		}

		public void setAdminId(String adminId) {
			this.adminId = adminId;
		}

		

}
