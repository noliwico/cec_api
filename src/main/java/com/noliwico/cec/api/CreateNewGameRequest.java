package com.noliwico.cec.api;

public class CreateNewGameRequest {
	private String displayName;
	private int maxPlayers;
	
	public CreateNewGameRequest() {
		this.displayName = "";
		this.maxPlayers = 4;
	}
	
	public CreateNewGameRequest(String displayName, int maxPlayers) {
		this.displayName = displayName;
		this.maxPlayers = maxPlayers;
	}
	
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public int getMaxPlayers() {
		return maxPlayers;
	}
	public void setMaxPlayers(int maxPlayers) {
		this.maxPlayers = maxPlayers;
	}
	
	
	
}
