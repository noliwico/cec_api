package com.noliwico.cec.api;

public class DoStepReply extends BaseReply {
	private boolean stepSuccess;
	
	public DoStepReply() {
		this.stepSuccess = true;
	}
	
	public DoStepReply( boolean stepSuccess) {
		super();
		this.stepSuccess = stepSuccess;
	}
	

	public boolean isStepSuccess() {
		return stepSuccess;
	}
	public void setStepSuccess(boolean stepSuccess) {
		this.stepSuccess = stepSuccess;
	}
	
	


}
