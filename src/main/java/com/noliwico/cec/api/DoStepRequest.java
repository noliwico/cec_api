package com.noliwico.cec.api;

public class DoStepRequest {
	private String gameId;
	private String playerId;
	private String playerSecret;
	private StepDetails step;
	
	public DoStepRequest() {
		
	}
	

	public String getGameId() {
		return gameId;
	}


	public void setGameId(String gameId) {
		this.gameId = gameId;
	}


	public String getPlayerId() {
		return playerId;
	}


	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}


	public String getPlayerSecret() {
		return playerSecret;
	}


	public void setPlayerSecret(String playerSecret) {
		this.playerSecret = playerSecret;
	}


	public StepDetails getStep() {
		return step;
	}
	public void setStep(StepDetails step) {
		this.step = step;
	}
	
	


}
