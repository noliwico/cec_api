package com.noliwico.cec.api;

import java.util.ArrayList;

public class GameStatus {

	
	private String gameId;
	private String gameName;
	private int maxPlayers;
	private ArrayList<CubeDetails> cubes;
	private int status;
	private int boardWidth;
	private int boardHeight;
	private String nextPlayerId;
	
	public GameStatus() {
		
	}
	
	
	public int getBoardWidth() {
		return boardWidth;
	}


	public void setBoardWidth(int boardWidth) {
		this.boardWidth = boardWidth;
	}


	public int getBoardHeight() {
		return boardHeight;
	}


	public void setBoardHeight(int boardHeight) {
		this.boardHeight = boardHeight;
	}


	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

	public String getGameId() {
		return gameId;
	}
	public void setGameId(String gameId) {
		this.gameId = gameId;
	}
	public String getGameName() {
		return gameName;
	}
	public void setGameName(String gameName) {
		this.gameName = gameName;
	}
	public int getMaxPlayers() {
		return maxPlayers;
	}
	public void setMaxPlayers(int maxPlayers) {
		this.maxPlayers = maxPlayers;
	}
	public ArrayList<CubeDetails> getCubes() {
		return cubes;
	}
	public void setCubes(ArrayList<CubeDetails> cubes) {
		this.cubes = cubes;
	}


	public String getNextPlayerId() {
		return nextPlayerId;
	}


	public void setNextPlayerId(String nextPlayerId) {
		this.nextPlayerId = nextPlayerId;
	}


	
}
