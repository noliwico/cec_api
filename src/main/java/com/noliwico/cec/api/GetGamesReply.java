package com.noliwico.cec.api;

import java.util.ArrayList;

public class GetGamesReply extends BaseReply {
	private ArrayList<GameStatus> games;
	
	public GetGamesReply() {
		super();
	}

	public GetGamesReply(ArrayList<GameStatus> games) {
		super();
		this.games = games;
	}

	public ArrayList<GameStatus> getGames() {
		return games;
	}

	public void setGames(ArrayList<GameStatus> games) {
		this.games = games;
	}
	
	

}
