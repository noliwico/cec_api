package com.noliwico.cec.api;

import java.util.ArrayList;

public class GetHistoryReply extends BaseReply {
	private ArrayList<GameStatus> historyItems;
	
	public GetHistoryReply() {
		super();
	}

	public ArrayList<GameStatus> getHistoryItems() {
		return historyItems;
	}

	public void setHistoryItems(ArrayList<GameStatus> historyItems) {
		this.historyItems = historyItems;
	}
	
	

}
