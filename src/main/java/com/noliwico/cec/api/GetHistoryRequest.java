package com.noliwico.cec.api;

public class GetHistoryRequest {
	private String gameId;
	private int from;

	public GetHistoryRequest() {
		
	}
	
	
	public int getFrom() {
		return from;
	}


	public void setFrom(int from) {
		this.from = from;
	}


	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}
	
	
}
