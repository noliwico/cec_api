package com.noliwico.cec.api;

public class GetStatusReply extends BaseReply {
	private GameStatus gameStatus;

	public GetStatusReply() {
		super();
	}
	public GameStatus getGameStatus() {
		return gameStatus;
	}

	public void setGameStatus(GameStatus game) {
		this.gameStatus = game;
	}
	
	

}
