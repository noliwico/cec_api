package com.noliwico.cec.api;

public class GetStatusRequest {
	private String gameId;
	private String playerId;
	private String playerSecret;
	
	public GetStatusRequest() {
		
	}

	public GetStatusRequest(String gameId, String playerId, String playerSecret) {
		super();
		this.gameId = gameId;
		this.playerId = playerId;
		this.playerSecret = playerSecret;
	}

	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}

	public String getPlayerId() {
		return playerId;
	}

	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}

	public String getPlayerSecret() {
		return playerSecret;
	}

	public void setPlayerSecret(String playerSecret) {
		this.playerSecret = playerSecret;
	}
	

	


}
