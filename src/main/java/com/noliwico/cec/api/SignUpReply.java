package com.noliwico.cec.api;

public class SignUpReply extends BaseReply {
	private String gameId;
	private String playerId;
	private String playerSecret;
	private int boardWidth;
	private int boardHeight;
	
	public SignUpReply() {
		super();
	}
	

	public String getGameId() {
		return gameId;
	}


	public void setGameId(String gameId) {
		this.gameId = gameId;
	}


	public String getPlayerId() {
		return playerId;
	}


	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}


	public String getPlayerSecret() {
		return playerSecret;
	}


	public void setPlayerSecret(String playerSecret) {
		this.playerSecret = playerSecret;
	}


	public int getBoardWidth() {
		return boardWidth;
	}
	public void setBoardWidth(int boardWidth) {
		this.boardWidth = boardWidth;
	}
	public int getBoardHeight() {
		return boardHeight;
	}
	public void setBoardHeight(int boardHeight) {
		this.boardHeight = boardHeight;
	}
	
	


}
