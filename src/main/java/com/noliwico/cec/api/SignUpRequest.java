package com.noliwico.cec.api;

public class SignUpRequest {
	private String gameId;
	private String playerName;
	
	public SignUpRequest() {
		
	}
	

	public SignUpRequest(String gameId, String playerName) {
		super();
		this.gameId = gameId;
		this.playerName = playerName;
	}


	public String getGameId() {
		return gameId;
	}


	public void setGameId(String gameId) {
		this.gameId = gameId;
	}


	public String getPlayerName() {
		return playerName;
	}
	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
	
	

}
