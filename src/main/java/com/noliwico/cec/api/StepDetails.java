package com.noliwico.cec.api;

public class StepDetails {
	private int dX;
	private int dY;
	
	public StepDetails() {
		
	}
	
	public StepDetails(int dX, int dY) {
		this.dX = dX;
		this.dY = dY;
	}
	public int getdX() {
		return dX;
	}
	public void setdX(int dX) {
		this.dX = dX;
	}
	public int getdY() {
		return dY;
	}
	public void setdY(int dY) {
		this.dY = dY;
	}
	
	

}
